-- AlterTable
ALTER TABLE `Image` MODIFY `region` VARCHAR(191) NOT NULL DEFAULT 'ap-northeast-1';

-- AlterTable
ALTER TABLE `Post` ADD COLUMN `readCount` INTEGER NOT NULL DEFAULT 0,
    MODIFY `text` TEXT NULL;
