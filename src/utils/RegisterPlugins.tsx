import { Editor } from "tinymce";
import MediaLibraryPlugin from "./MediaLibraryPlugin";

export type PluginRegistrationProps = {
  editor: Editor;
};

const RegisterPlugins = ({ ...props }: PluginRegistrationProps) => {
  MediaLibraryPlugin({ ...props });
};

export default RegisterPlugins;
