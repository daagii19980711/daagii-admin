import { NextRequest } from "next/server";

export const getPathValue = (request: NextRequest, baseUrl: string) => {
  return request.url.split(`${baseUrl}/`)[1];
};
