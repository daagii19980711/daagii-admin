import { ImagePicker } from "@admin/components/image-picker";
import { PluginRegistrationProps } from "./RegisterPlugins";
import { Image } from "@prisma/client";
import { createRoot } from "react-dom/client";
import { getImageUrl } from "./get-image-url";

const MediaLibraryPlugin = ({ editor }: PluginRegistrationProps) => {
  editor.ui.registry.addButton("medialibrary", {
    text: "Зургын сан",
    icon: "gallery",
    onAction: function () {
      const dialogContainer = document.createElement("div");
      document.body.appendChild(dialogContainer);
      const root = createRoot(dialogContainer);

      const closeDialog = () => {
        root.unmount();
        dialogContainer.remove();
      };

      const onClickImage = (image: Image) => {
        editor.insertContent(`<img src="${getImageUrl(image, "original")}" />`);
        closeDialog();
      };

      root.render(<ImagePicker onClick={onClickImage} onClose={closeDialog} />);
    },
  });
};

export default MediaLibraryPlugin;
