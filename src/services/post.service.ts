import { Image, PostSection, Prisma } from "@prisma/client";
import prisma from "../utils/db";
import { getPostSection } from "./post-section.service";
import { getImage } from "./image.service";

interface FilterParams {
  query?: string;
  section?: string | null;
}
interface GetAllPostsParams extends FilterParams {
  order?: "asc" | "desc";
  orderColumn?: string;
  limit?: number;
  skip?: number;
}
export const getPost = async (id: string) => {
  try {
    const result = await prisma.post.findUnique({ where: { id }, include: { section: true, image: true } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getAllPosts = async ({ order = "desc", orderColumn = "createdAt", limit = 1000, skip = 0, query = "", section = null }: GetAllPostsParams) => {
  try {
    const result = await prisma.post.findMany({
      select: {
        id: true,
        title: true,
        image: true,
        description: true,
        section: true,
        readCount: true,
        createdAt: true,
        updatedAt: true,
      },
      orderBy: {
        [orderColumn]: order,
      },
      where: {
        AND: [
          {
            OR: [
              {
                title: {
                  contains: query,
                },
              },
              {
                description: {
                  contains: query,
                },
              },
            ],
          },
          {
            section: section ? { id: section } : undefined,
          },
        ],
      },
      skip,
      take: limit,
    });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getPostCount = async ({ query = "", section = null }: FilterParams): Promise<number> => {
  try {
    const count = await prisma.post.count({
      where: {
        AND: [
          {
            OR: [
              {
                title: {
                  contains: query,
                },
              },
              {
                description: {
                  contains: query,
                },
              },
            ],
          },
          {
            section: section ? { id: section } : undefined,
          },
        ],
      },
    });

    return count;
  } catch (error) {
    throw error;
  }
};

export const createPost = async (data: Prisma.PostCreateInput) => {
  try {
    const result = await prisma.post.create({ data, include: { section: true } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const updatePost = async (id: string, data: Prisma.PostUncheckedUpdateInput) => {
  try {
    let section: PostSection | null = null;
    let image: Image | null = null;
    const { sectionId, imageId, ...updatingPost } = data;
    if (sectionId) {
      const { result: sectionResult } = await getPostSection(sectionId.toString());
      if (sectionResult) section = sectionResult;
    }
    if (!section) return { error: "Updating section is invalid" };

    const { result: imageResult } = await getImage(imageId?.toString() || "");
    if (imageResult) image = imageResult;
    const updatingData: any = updatingPost;
    updatingData.section = { connect: { id: section.id } };
    updatingData.image = image ? { connect: { id: image.id } } : { disconnect: true };
    console.log("updatingData:", updatingData);
    delete updatingData["slug"];
    delete updatingData["id"];
    const result = await prisma.post.update({ where: { id }, data: updatingData, include: { section: true } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const deletePost = async (id: string) => {
  try {
    const result = await prisma.post.delete({ where: { id } });
    return { result: result.id };
  } catch (error) {
    return { error };
  }
};

export const increaseReadCount = async (id: string) => {
  try {
    const result = await prisma.post.update({
      where: { id },
      data: {
        readCount: {
          increment: 1,
        },
      },
    });
    return { result: result.id };
  } catch (error) {
    return { error };
  }
};
