import { Prisma } from "@prisma/client";
import prisma from "../utils/db";

export const getUser = async (id: string) => {
  try {
    const result = await prisma.user.findUnique({ where: { id } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getUserByEmail = async (email: string) => {
  try {
    const result = await prisma.user.findUnique({ where: { email } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getAllUsers = async () => {
  try {
    const result = await prisma.user.findMany({ orderBy: { loggedAt: "desc" } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const authUser = async (data: Prisma.UserCreateInput) => {
  try {
    const { result: user } = await getUserByEmail(data.email);

    if (!user) {
      const result = await prisma.user.create({ data });
      return { result };
    }
    const result = await prisma.user.update({ where: { id: user.id }, data: { loggedAt: new Date() } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const toggleUserAdmin = async (id: string) => {
  try {
    const { result: user } = await getUser(id);
    if (user) {
      const result = await prisma.user.update({ where: { id }, data: { isAdmin: !user.isAdmin } });
      return { result };
    }
    return { result: user };
  } catch (error) {
    return { error };
  }
};

export const deleteUser = async (id: string) => {
  try {
    const result = await prisma.user.delete({ where: { id } });
    return { result: result.id };
  } catch (error) {
    return { error };
  }
};
