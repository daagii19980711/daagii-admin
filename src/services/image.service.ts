import { Prisma } from "@prisma/client";
import prisma from "../utils/db";

export const getImage = async (id: string) => {
  try {
    const result = await prisma.image.findUnique({ where: { id } });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const getAllImages = async () => {
  try {
    const result = await prisma.image.findMany();
    return { result };
  } catch (error) {
    return { error };
  }
};

export const createImage = async (rawData: Prisma.ImageCreateInput) => {
  try {
    const data = { ...rawData };
    data.key = data.key.replaceAll(data.id, "");
    const result = await prisma.image.create({ data });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const updateImage = async (id: string, data: Prisma.ImageUpdateInput) => {
  try {
    const result = await prisma.image.update({ where: { id }, data });
    return { result };
  } catch (error) {
    return { error };
  }
};

export const deleteImage = async (id: string) => {
  try {
    const result = await prisma.image.delete({ where: { id } });
    return { result: result.id };
  } catch (error) {
    return { error };
  }
};
