"use client";
import { useEffect, useRef, useState } from "react";
import { useRouter } from "next/navigation";
import { useUser } from "@admin/hooks/use-user";
import axios from "axios";
import { User } from "@prisma/client";

interface AuthGuardProps {
  children: any;
}

export const AuthGuard = (props: AuthGuardProps) => {
  const { children } = props;
  const router = useRouter();
  const { setUser } = useUser();

  useEffect(() => {
    axios
      .get("/api/auth/me")
      .then((response) => {
        const user = response.data.user as User;
        setUser(user);
      })
      .catch((error) => {
        router.replace("/auth/login");
      });
  }, []);

  return children;
};
