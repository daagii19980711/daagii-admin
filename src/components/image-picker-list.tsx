import { BaseTableProps } from "@admin/types";
import { getImageUrl } from "@admin/utils/get-image-url";
import { ArrowUpTrayIcon } from "@heroicons/react/24/solid";
import { Button, Card, CardMedia, Grid, Pagination, Stack, SvgIcon, Typography } from "@mui/material";
import { Image } from "@prisma/client";
import { nanoid } from "nanoid";

interface ImagePickerProps extends BaseTableProps {
  onClick: (item: Image) => void;
  onCreateClick: () => void;
}

export const ImagePickerList: React.FC<ImagePickerProps> = (props) => {
  const { pageItems = [], onClick, onCreateClick, page = 0, totalItems = 0, rowsPerPage = 5, handlePageChange = () => {} } = props;

  return (
    <Stack sx={{ margin: 2 }} rowGap={2}>
      <Stack flexDirection={"row"} alignItems={"center"} justifyContent={"space-between"}>
        <Typography variant="h5">Зургын сан</Typography>
        <Button
          onClick={() => {
            onCreateClick();
          }}
        >
          <SvgIcon>
            <ArrowUpTrayIcon />
          </SvgIcon>
        </Button>
      </Stack>
      <Grid container rowGap={2} columnGap={2}>
        {pageItems.map((row) => (
          <Grid item md={2} sm={4} xs={6} key={nanoid()}>
            <Card
              sx={{ border: 0, cursor: "pointer" }}
              onClick={() => {
                onClick(row);
              }}
            >
              <CardMedia component="img" height={100} image={getImageUrl(row)} alt="Image" />
            </Card>
          </Grid>
        ))}
      </Grid>
      <Pagination
        sx={{ display: "flex", justifyContent: "flex-end" }}
        onChange={(_, page) => {
          handlePageChange(page - 1);
        }}
        page={page}
        count={Math.ceil(totalItems / rowsPerPage)}
        shape="rounded"
      />
    </Stack>
  );
};
