import { Button, Box, Card, Stack, SvgIcon, Table, TableBody, TableCell, TableHead, TablePagination, TableRow, Typography, Tooltip, IconButton } from "@mui/material";
import { Scrollbar } from "../scrollbar";
import { PencilSquareIcon, TrashIcon } from "@heroicons/react/20/solid";
import { useQuery } from "@admin/hooks/use-query";
import { BaseTableProps } from "@admin/types";
import { FormatDateAsSince } from "../format-as-since";

export const CategoriesTable: React.FC<BaseTableProps> = (props) => {
  const { totalItems = 0, pageItems = [], handlePageChange = () => {}, handleRowsPerPageChange, page = 0, rowsPerPage = 0 } = props;
  const { addQuery } = useQuery();

  const handleEdit = (id: string) => {
    addQuery("editing", id);
  };

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell width={1}>#</TableCell>
                <TableCell>Нэр</TableCell>
                <TableCell width={1}>Үүсгэсэн</TableCell>
                <TableCell align="center" width={1}>
                  Үйлдэл
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {pageItems.map((row, index) => {
                return (
                  <TableRow hover key={row.id}>
                    <TableCell>
                      <Typography variant="subtitle2">{index + 1}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography variant="subtitle2">
                        {row.title}&nbsp;
                        <Typography variant="caption" fontSize={10}>
                          ({row.slug})
                        </Typography>
                      </Typography>
                    </TableCell>
                    <TableCell sx={{ whiteSpace: "nowrap" }}>
                      <Typography variant="subtitle2">
                        <FormatDateAsSince value={row.createdAt} />
                      </Typography>
                    </TableCell>

                    <TableCell sx={{ whiteSpace: "nowrap" }}>
                      <Stack direction={"row"} gap={2}>
                        <Tooltip title={"Засах"}>
                          <IconButton
                            color="info"
                            size="small"
                            onClick={() => {
                              handleEdit(row.id);
                            }}
                          >
                            <SvgIcon>
                              <PencilSquareIcon />
                            </SvgIcon>
                          </IconButton>
                        </Tooltip>
                        <Tooltip title={"Устгах"}>
                          <IconButton color="error" size="small" onClick={() => addQuery("deleting", row.id)}>
                            <SvgIcon>
                              <TrashIcon />
                            </SvgIcon>
                          </IconButton>
                        </Tooltip>
                      </Stack>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        labelRowsPerPage="Нэг хуудсанд"
        labelDisplayedRows={({ from, to, count }) => {
          return (
            <>
              {from}-{to} нийт {count}
            </>
          );
        }}
        component="div"
        count={totalItems}
        onPageChange={(_, page) => {
          if (handlePageChange) handlePageChange(page);
        }}
        onRowsPerPageChange={(e: any) => {
          if (handleRowsPerPageChange) handleRowsPerPageChange(e.target.value);
        }}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};
