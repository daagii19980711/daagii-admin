import { usePage } from "@admin/hooks/use-page";
import { Dialog, DialogContent } from "@mui/material";
import { Image } from "@prisma/client";
import { ImagePickerList } from "./image-picker-list";
import { useMemo, useState } from "react";
import { applyPagination } from "@admin/utils/apply-pagination";
import useSWR, { useSWRConfig } from "swr";
import { fetcher } from "@admin/utils/fetcher";
import { ImagesCreate } from "./images/images-create";

interface ImagePickerProps {
  onClick: (item: Image) => void;
  onClose: () => void;
}

const usePageItems = (data: any[], page: number, rowsPerPage: number) => {
  return useMemo(() => {
    return applyPagination(data, page, rowsPerPage);
  }, [page, rowsPerPage, data]);
};

export const ImagePicker: React.FC<ImagePickerProps> = (props) => {
  const { onClick, onClose } = props;
  const [creating, setCreating] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const { data = [] } = useSWR("/api/files", fetcher);
  const pageItems = usePageItems(data, page, rowsPerPage);
  const { mutate } = useSWRConfig();

  const paginationProps = { page, handlePageChange: setPage, rowsPerPage, totalItems: data.length, handleRowsPerPageChange: setRowsPerPage };

  return (
    <>
      <Dialog fullWidth maxWidth={"sm"} open onClose={onClose}>
        <DialogContent>
          {!creating ? (
            <ImagePickerList onClick={onClick} onCreateClick={() => setCreating(true)} pageItems={pageItems} {...paginationProps} />
          ) : (
            <ImagesCreate
              onClose={() => setCreating(false)}
              onSubmit={() => {
                mutate("/api/files");
                setCreating(false);
              }}
            />
          )}
        </DialogContent>
      </Dialog>
    </>
  );
};
