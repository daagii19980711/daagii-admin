import React, { useState } from "react";
import { Box, Button, FormControl, FormHelperText, Stack } from "@mui/material";
import { Image } from "@prisma/client";
import { ImagePicker } from "../image-picker";
import { getImageUrl } from "@admin/utils/get-image-url";

interface ImageSelectFieldProps {
  value?: Image;
  onChange: (value: Image | undefined) => void;
  error?: string;
}

export const ImageSelectField: React.FC<ImageSelectFieldProps> = (props) => {
  const { value, onChange, error } = props;
  const [selecting, setSelecting] = useState(false);
  const imageUrl = value ? getImageUrl(value, "extra_small") : "https://via.placeholder.com/100x100";
  return (
    <>
      <FormControl error={Boolean(error)}>
        <Stack direction={"row"} alignItems={"center"} gap={2}>
          <Box
            onClick={() => {
              setSelecting(true);
            }}
            sx={{ width: "100px", height: "100px", cursor: "pointer", position: "relative", borderRadius: "15px", border: "1px solid #ccc", overflow: "hidden" }}
          >
            <img src={imageUrl} alt="" style={{ position: "absolute", left: 0, top: 0, width: "100%", height: "100%", objectFit: "contain" }} />
          </Box>
          {value && (
            <Button variant="text" color="error" onClick={() => onChange(undefined)}>
              Зураггүй
            </Button>
          )}
        </Stack>
        {error && <FormHelperText>{error}</FormHelperText>}
      </FormControl>
      {selecting && (
        <ImagePicker
          onClick={(image) => {
            onChange(image);
            setSelecting(false);
          }}
          onClose={() => {
            setSelecting(false);
          }}
        />
      )}
    </>
  );
};
