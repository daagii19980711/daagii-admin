import RegisterPlugins from "@admin/utils/RegisterPlugins";
import { Box, FormControl, FormHelperText } from "@mui/material";
import { Editor } from "@tinymce/tinymce-react";
import { Editor as TinyMCEEditor } from "tinymce/tinymce";

interface RichTextFieldProps {
  error?: string;
  value?: string;
  onChange?: (value: string) => void;
}

export const RichTextField: React.FC<RichTextFieldProps> = (props) => {
  const setup = (editor: TinyMCEEditor) => {
    RegisterPlugins({ editor });
  };
  const { error, value, onChange } = props;
  return (
    <FormControl fullWidth error={Boolean(error)}>
      <Box>
        <Editor
          id={"xb902cuimr03w70cjrtn8zw3ua3zzclpisqmplfh55467cgp"}
          value={value}
          apiKey="xb902cuimr03w70cjrtn8zw3ua3zzclpisqmplfh55467cgp"
          initialValue=""
          onInit={(_, editor) => {
            if (!editor.getContent()) {
              editor.setContent(value || "");
            }
          }}
          onEditorChange={onChange}
          plugins={["medialibrary", "paste", "lists", "image", "code", "imagetools", "link", "template", "table"]}
          init={{
            branding: false,
            height: 600,
            menubar: true,
            image_caption: true,
            plugins: "print preview paste searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern",
            toolbar: "formatselect | bold italic underline strikethrough | forecolor backcolor blockquote | link image medialibrary media | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent removeformat ",
            image_advtab: true,
            setup,
          }}
        />
      </Box>
      {error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
};
