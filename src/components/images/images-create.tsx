import { Button, Card, CardMedia, CardActions, CardContent, Dialog, DialogActions, DialogContent, DialogTitle, Typography, IconButton, Skeleton, Box, SvgIcon, CircularProgress } from "@mui/material";
import { useEffect, useState } from "react";
import { useSWRConfig } from "swr";
import axios from "axios";
import { CheckIcon, TrashIcon } from "@heroicons/react/24/solid";
import { useDropzone } from "react-dropzone";
import { useToast } from "@admin/hooks/use-toast";
import Grid from "@mui/material/Unstable_Grid2"; // Grid version 2
import { nanoid } from "nanoid";

interface ImagesCreateProps {
  onSubmit: () => void;
  onClose: (data: any) => void;
}

export const ImagesCreate: React.FC<ImagesCreateProps> = (props) => {
  const { onSubmit, onClose } = props;
  const { mutate } = useSWRConfig();
  const toast = useToast();
  const [files, setFiles] = useState<Array<File>>([]);
  const [uploading, setUploading] = useState(false);
  const [uploaded, setUploaded] = useState(false);
  const { getRootProps, getInputProps, acceptedFiles, fileRejections } = useDropzone({
    accept: {
      "image/jpg": [".jpg"],
      "image/jpeg": [".jpeg"],
      "image/webp": [".webp"],
      "image/png": [".png"],
    },
    maxSize: 5 * 1024 * 1024,
  });

  useEffect(() => {
    if (fileRejections.length > 0) {
      for (const rejection of fileRejections) {
        const errorMsg = rejection.errors[0].message;
        toast.error(errorMsg);
      }
    }
  }, [fileRejections]);

  useEffect(() => {
    setFiles(acceptedFiles);
  }, [acceptedFiles]);

  const removeImage = (file: File) => {
    setFiles(files.filter((item) => item !== file));
  };

  const uploadImages = async () => {
    setUploading(true);
    try {
      await Promise.all(
        files.map(async (file) => {
          const formData = new FormData();
          formData.append("file", file);

          const response = await axios.post("/api/files", formData);
          console.log("response:", response);
        })
      );
    } catch (error) {
      console.error("Error uploading files:", error);
    }
    setUploading(false);
    setUploaded(true);
    onSubmit();
    mutate("/api/file");
  };

  const UploadingImageItem: React.FC<{ file: File }> = ({ file }) => {
    const [base64, setBase64] = useState("");
    useEffect(() => {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        setBase64(reader.result as string);
      };
    }, [file]);
    return (
      <Card sx={{ position: "relative" }}>
        {!base64 && <Skeleton width={"100%"} height={194} />}
        {base64 && <CardMedia component="img" height="194" image={base64} alt="Paella dish" />}
        <CardActions sx={{ position: "absolute", right: "10px", top: "10px" }}>
          {!uploaded && !uploading && (
            <IconButton color="error" onClick={() => removeImage(file)}>
              <SvgIcon fontSize="small">
                <TrashIcon />
              </SvgIcon>
            </IconButton>
          )}
          {uploading && (
            <IconButton color="default" disabled>
              <CircularProgress size={10} />
            </IconButton>
          )}
          {uploaded && (
            <IconButton color="success" disabled>
              <SvgIcon fontSize="small">
                <CheckIcon />
              </SvgIcon>
            </IconButton>
          )}
        </CardActions>
      </Card>
    );
  };

  return (
    <Dialog open={true} onClose={onClose} maxWidth={"sm"} fullWidth>
      <DialogTitle id="alert-dialog-title">Зураг үүсгэх</DialogTitle>
      <DialogContent>
        <Box
          {...getRootProps()}
          sx={{
            cursor: "pointer",
            display: "flex",
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            padding: "10px",
            border: "2px dashed #CCC",
            borderRadius: "5px",
            backgroundColor: "#F0F0F0",
            color: "#888",
            outline: "none",
            transition: "300ms",
            marginBottom: "20px",
            "&:hover": {
              border: "2px solid #CCC",
            },
          }}
        >
          <input {...getInputProps()} />
          <Typography variant="subtitle2" textAlign={"center"}>
            Оруулах зургаа сонгоно уу, эсвэл энд чирнэ үү!
            <br /> ( Файлын дээд хэмжээ 5MB )
          </Typography>
        </Box>
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          {files.map((file) => (
            <Grid xs={4} key={nanoid()}>
              <UploadingImageItem file={file} key={nanoid()} />
            </Grid>
          ))}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} type="button">
          Болих
        </Button>
        <Button variant="contained" autoFocus onClick={uploadImages}>
          Үүсгэх
        </Button>
      </DialogActions>
    </Dialog>
  );
};
