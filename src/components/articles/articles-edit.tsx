import Card from "@mui/material/Card";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import SvgIcon from "@mui/material/SvgIcon";
import TextField from "@mui/material/TextField";
import * as Yup from "yup";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import axios from "axios";
import { PencilIcon, XMarkIcon } from "@heroicons/react/24/solid";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { FormHelperText } from "@mui/material";
import { RichTextField } from "../form/rich-text-field";
import { Image, Post, PostSection } from "@prisma/client";
import { ImageSelectField } from "../form/image-select-field";

interface ExtendedPost extends Post {
  image: Image;
}
interface ArticlesEditProps {
  onSubmit: () => void;
  onClose: () => void;
  sections: Array<PostSection>;
  values: ExtendedPost;
}
export const ArticlesEdit: React.FC<ArticlesEditProps> = (props) => {
  const { onSubmit, onClose, values: rawValues, sections = [] } = props;
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState<Image | undefined>(undefined);
  const formik = useFormik({
    initialValues: {
      title: rawValues.title,
      description: rawValues.description,
      sectionId: rawValues.sectionId,
      text: rawValues.text,
      imageId: "",
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required("Нэр хоосон байсан болохгүй"),
      sectionId: Yup.string().required("Ангилал хоосон байсан болохгүй"),
    }),
    onSubmit: async (values, helpers) => {
      try {
        setLoading(true);
        const { data } = await axios.patch(`/api/posts/${rawValues.id}`, values);
        onSubmit();
        setLoading(false);
      } catch (err: any) {}
    },
  });
  useEffect(() => {
    setImage(rawValues.image);
    if (window) {
    }
  }, []);
  useEffect(() => {
    formik.setFieldValue("imageId", image?.id);
  }, [image]);

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <Stack spacing={1}>
        <Stack direction={"row"} justifyContent={"space-between"}>
          <Typography variant="h4">Мэдээ мэдээлэл засах</Typography>

          <Stack direction={"row"} gap={3} alignSelf={"flex-end"}>
            <Button
              onClick={onClose}
              type="button"
              startIcon={
                <SvgIcon fontSize="small">
                  <XMarkIcon />
                </SvgIcon>
              }
            >
              Болих
            </Button>
            <Button
              type="submit"
              variant="contained"
              startIcon={
                <SvgIcon fontSize="small">
                  <PencilIcon />
                </SvgIcon>
              }
            >
              Засах
            </Button>
          </Stack>
        </Stack>
        <Card>
          <Stack p={3}>
            <Stack rowGap={3}>
              <TextField name="title" error={!!(formik.touched.title && formik.errors.title)} fullWidth helperText={formik.touched.title && formik.errors.title} label="Нэр" onBlur={formik.handleBlur} onChange={formik.handleChange} type="text" value={formik.values.title} />
              <ImageSelectField onChange={setImage} value={image} error={formik.touched.imageId ? formik.errors.imageId : undefined} />
              <Stack direction={"row"} gap={3}>
                <FormControl fullWidth error={!!(formik.touched.sectionId && formik.errors.sectionId)}>
                  <InputLabel>Ангилал</InputLabel>
                  <Select labelId="sectionId-label" id="sectionId" value={formik.values.sectionId} onChange={formik.handleChange} onBlur={formik.handleBlur} name="sectionId">
                    <MenuItem value={""}>Сонгох...</MenuItem>
                    {sections.map((section) => (
                      <MenuItem key={section.id} value={section.id} selected={section.id === formik.values.sectionId}>
                        {section.title}
                      </MenuItem>
                    ))}
                  </Select>
                  {formik.touched.sectionId && formik.errors.sectionId && <FormHelperText>{formik.errors.sectionId}</FormHelperText>}
                </FormControl>
              </Stack>
              <TextField maxRows={3} minRows={3} name="description" multiline error={!!(formik.touched.description && formik.errors.description)} fullWidth helperText={formik.touched.description && formik.errors.description} label="Тайлбар" onBlur={formik.handleBlur} onChange={formik.handleChange} value={formik.values.description} />
              <RichTextField
                error={formik.touched.title ? formik.errors.text : undefined}
                onChange={(value) => {
                  formik.setFieldValue("text", value);
                }}
                value={formik.values.text || ""}
              />
            </Stack>
          </Stack>
        </Card>
      </Stack>
    </form>
  );
};
