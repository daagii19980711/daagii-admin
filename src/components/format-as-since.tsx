import dayjs from "dayjs";
import React from "react";

var relativeTime = require("dayjs/plugin/relativeTime");
dayjs.extend(relativeTime);

interface Props {
  value: Date;
}
export const FormatDateAsSince: React.FC<Props> = (props) => {
  const { value } = props;
  //@ts-ignore
  let fromString = dayjs(value).fromNow();

  fromString = fromString.replaceAll("a few seconds ago", "саяхан");
  fromString = fromString.replaceAll("a minute", "1 минутын");
  fromString = fromString.replaceAll("minutes", "минутын");
  fromString = fromString.replaceAll("an hour", "1 цагын");
  fromString = fromString.replaceAll("hours", "цагын");
  fromString = fromString.replaceAll("a day", "өчигдөр");
  fromString = fromString.replaceAll("days", "өдрийн");
  fromString = fromString.replaceAll("a month", "1 сарын");
  fromString = fromString.replaceAll("months", "сарын");
  fromString = fromString.replaceAll("a year", "1 жилын");
  fromString = fromString.replaceAll("years", "жилын");
  fromString = fromString.replaceAll("ago", "өмнө");

  return <>{fromString}</>;
};
