import { Box, Card, Table, TableBody, TableCell, TableHead, TablePagination, TableRow, Typography, Tooltip, IconButton, Avatar, Switch } from "@mui/material";
import { Scrollbar } from "../scrollbar";
import { BaseTableProps } from "@admin/types";
import { FormatDateAsSince } from "../format-as-since";
import { User } from "@prisma/client";
import axios from "axios";
import { useSWRConfig } from "swr";
import { useToast } from "@admin/hooks/use-toast";

export const UsersTable: React.FC<BaseTableProps> = (props) => {
  const { totalItems = 0, pageItems = [], handlePageChange = () => {}, handleRowsPerPageChange, page = 0, rowsPerPage = 0 } = props;
  const { mutate } = useSWRConfig();
  const toast = useToast();

  const toggleUserAdmin = (id: string) => {
    if (confirm("Уг хэрэглэгчийг админ болгохдоо итгэлтэй байна уу?")) {
      axios
        .patch(`/api/users/admin/${id}`)
        .then((res) => {
          const user = res.data.result;
          toast.success(`Амжилттай ${user.isAdmin ? "админ боллоо" : "админг хураалаа"}`);
          mutate("/api/users");
        })
        .catch(() => {
          toast.error(`Алдаа гарлаа`);
        });
    }
  };

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell width={1}>#</TableCell>
                <TableCell width={"50px"}>Зураг</TableCell>
                <TableCell>Нэр</TableCell>
                <TableCell width={1}>Нэвтэрсэн</TableCell>
                <TableCell align="center" width={1}>
                  Админ
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {pageItems.map((row: User, index) => {
                return (
                  <TableRow hover key={row.id}>
                    <TableCell>
                      <Typography variant="subtitle2">{index + 1}</Typography>
                    </TableCell>
                    <TableCell>
                      <Avatar alt={row.name} src={row.image} />
                    </TableCell>
                    <TableCell>
                      <Typography variant="subtitle2">{row.name}&nbsp;</Typography>
                    </TableCell>
                    <TableCell sx={{ whiteSpace: "nowrap" }}>
                      <Typography variant="subtitle2">
                        <FormatDateAsSince value={row.loggedAt} />
                      </Typography>
                    </TableCell>

                    <TableCell sx={{ whiteSpace: "nowrap" }}>
                      <Switch
                        checked={row.isAdmin}
                        onChange={() => {
                          toggleUserAdmin(row.id);
                        }}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        labelRowsPerPage="Нэг хуудсанд"
        labelDisplayedRows={({ from, to, count }) => {
          return (
            <>
              {from}-{to} нийт {count}
            </>
          );
        }}
        component="div"
        count={totalItems}
        onPageChange={(_, page) => {
          if (handlePageChange) handlePageChange(page);
        }}
        onRowsPerPageChange={(e: any) => {
          if (handleRowsPerPageChange) handleRowsPerPageChange(e.target.value);
        }}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};
