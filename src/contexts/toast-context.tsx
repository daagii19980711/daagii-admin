"use client";
import { Alert, AlertColor, Box, Slide, SlideProps, Snackbar } from "@mui/material";
import { createContext, useContext, useState } from "react";

interface Snack {
  message?: string;
  color?: AlertColor;
  autoHideDuration?: number;
  open: boolean;
}

type SnackDefaultValue = {
  setSnack: React.Dispatch<React.SetStateAction<Snack>>;
};

export const ToastContext = createContext<SnackDefaultValue>({} as SnackDefaultValue);

interface ToastProviderProps {
  children: React.ReactNode;
}

type TransitionProps = Omit<SlideProps, "direction">;

function Transition(props: TransitionProps) {
  return <Slide {...props} direction="left" />;
}
export const ToastProvider: React.FC<ToastProviderProps> = ({ children }) => {
  const [snack, setSnack] = useState<Snack>({ open: false });
  const handleClose = (_: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setSnack({ open: false });
  };

  return (
    <ToastContext.Provider value={{ setSnack }}>
      {children}
      <Snackbar open={snack.open} autoHideDuration={snack.autoHideDuration} onClose={handleClose} anchorOrigin={{ vertical: "bottom", horizontal: "right" }} TransitionComponent={Transition}>
        <Box sx={{ boxShadow: 4 }}>
          <Alert severity={snack.color}>{snack.message || ""}</Alert>
        </Box>
      </Snackbar>
    </ToastContext.Provider>
  );
};
