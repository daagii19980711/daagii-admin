import { getUserByEmail } from "@admin/services/user.service";
import { getPathValue } from "@admin/utils/get-path-value";
import { NextRequest, NextResponse } from "next/server";

export async function GET(request: NextRequest) {
  try {
    const email = getPathValue(request, "user/email");
    const { result } = await getUserByEmail(email);
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}
