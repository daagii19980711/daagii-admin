import { getAllPosts } from "@admin/services/post.service";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest) {
  try {
    const url = new URL(req.url || "");
    const { searchParams } = url;
    const filteringQuery = Object.fromEntries(searchParams.entries());
    const { query, section, order, orderColumn, limit, skip } = filteringQuery;
    const { result, error } = await getAllPosts({ query, section, order: order as "asc" | "desc", orderColumn, limit: limit ? Number(limit) : 1000, skip: skip ? Number(skip) : 0 });
    if (error) throw error;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}
