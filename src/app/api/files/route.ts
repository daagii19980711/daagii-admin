import { writeFile } from "fs/promises";
import { nanoid } from "nanoid";
import { NextRequest, NextResponse } from "next/server";
import { dirname as pathDirname, join } from "path";
import sharp from "sharp";
import { PutObjectCommand } from "@aws-sdk/client-s3";
import { s3Client } from "@admin/utils/s3";
import { createImage, getAllImages } from "@admin/services/image.service";
import { existsSync, mkdirSync } from "fs";
const S3_BUCKET_NAME = process.env.AWS_S3_BUCKET || "";

export async function POST(request: NextRequest) {
  const currentDate = new Date();
  const year = currentDate.getFullYear().toString();
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based
  const day = currentDate.getDate().toString().padStart(2, "0");

  const data = await request.formData();
  const file: File | null = data.get("file") as unknown as File;

  if (!file) {
    return NextResponse.json({ success: false, error: "File not found" }, { status: 400 });
  }
  const bytes = await file.arrayBuffer();
  const buffer = Buffer.from(bytes);
  const fileId = nanoid();
  const outputDirectory = join("/", "tmp", "tecms", year, month, day);
  ensureDirectoryExistence(outputDirectory);
  const sourceFileExtension = getFileExtension(file.name);
  const sourceFilePath = join(outputDirectory, `${fileId}_source.${sourceFileExtension}`);
  await writeFile(sourceFilePath, buffer);

  const sizes = [
    { name: "large", width: 1200 },
    { name: "big", width: 720 },
    { name: "medium", width: 480 },
    { name: "small", width: 360 },
    { name: "extra_small", width: 240 },
  ];

  let files: any[] = [{ key: sourceFilePath, buffer }];
  await Promise.all(
    sizes.map(async (size) => {
      const outputPath = join(outputDirectory, `${fileId}_${size.name}.webp`);
      const output = await compressImage(sourceFilePath, outputPath, size);
      files.push(output);
    })
  );

  // Upload processed image files to S3
  await Promise.all(
    files.map(async (file) => {
      const command = new PutObjectCommand({
        Bucket: S3_BUCKET_NAME,
        Key: file.key.replaceAll("/tmp/tecms/", ""),
        Body: file.buffer,
      });

      try {
        await s3Client.send(command);
      } catch (err) {
        console.error("Error uploading file to S3:", err);
      }
    })
  );
  const defaultKey = join(outputDirectory, `${fileId}`).replaceAll("/tmp/tecms/", "");
  const { result } = await createImage({ id: fileId, key: defaultKey, bucket: S3_BUCKET_NAME, region: process.env.AWS_S3_REGION, originalExt: sourceFileExtension });
  return NextResponse.json({ result }, { status: 200 });
}

const compressImage = async (filePath: string, outputPath: string, size: { name: string; width: number }) =>
  new Promise((resolve, reject) => {
    sharp(filePath)
      .rotate()
      .resize(size)
      .webp({ quality: 80 })
      .toBuffer((err, buffer) => {
        if (err) {
          console.error("Error compressing image:", err);
          reject(err);
        } else {
          writeFile(outputPath, buffer).then(() => {
            resolve({ key: outputPath, buffer });
          });
        }
      });
  });

function ensureDirectoryExistence(filePath: string) {
  const dirname = pathDirname(filePath);
  if (existsSync(dirname)) {
    return true;
  }
  // Base case: If the parent directory already exists, return early
  if (!ensureDirectoryExistence(dirname)) {
    mkdirSync(filePath, { recursive: true });
  }
}

const getFileExtension = (filePath: string) => {
  const splittedName = filePath.split(".");
  return splittedName[splittedName.length - 1];
};

export async function GET() {
  try {
    const { result, error } = await getAllImages();
    if (error) throw error;

    return NextResponse.json({ result }, { status: 200 });
  } catch (error: any) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}
