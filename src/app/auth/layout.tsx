import { AuthLayout } from "@admin/layouts/auth/layout";

export default function Layout({ children }: any) {
  return <AuthLayout>{children}</AuthLayout>;
}
