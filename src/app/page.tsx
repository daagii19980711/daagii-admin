import { Box, Container, Typography } from "@mui/material";

export default function Home() {
  return (
    <>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl">
          <Typography>Hello admin</Typography>
        </Container>
      </Box>
    </>
  );
}
