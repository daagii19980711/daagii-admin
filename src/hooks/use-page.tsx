import { applyPagination } from "../utils/apply-pagination";
import { fetcher } from "@admin/utils/fetcher";
import { useCallback, useEffect, useMemo, useState } from "react";
import useSWR, { useSWRConfig } from "swr";
import { useQuery } from "./use-query";
import { useSearchParams } from "next/navigation";
import { useToast } from "./use-toast";
import axios from "axios";

const usePageItems = (data: any[], page: number, rowsPerPage: number) => {
  return useMemo(() => {
    return applyPagination(data, page, rowsPerPage);
  }, [page, rowsPerPage, data]);
};

export const usePage = (url: string, pageSize = 5) => {
  const [loading, setLoading] = useState(false);
  const [editingItem, setEditingItem] = useState<any>(undefined);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pageSize);
  const { data = [], isLoading } = useSWR(url, fetcher);
  const pageItems = usePageItems(data, page, rowsPerPage);
  const { addQuery, removeQuery } = useQuery();
  const searchParams = useSearchParams();
  const { mutate } = useSWRConfig();
  const toast = useToast();

  const editing = searchParams.get("editing");
  const deleting = searchParams.get("deleting");

  useEffect(() => {
    if (deleting) {
      if (confirm("Уг мэдээг усгахадаа итгэлтэй байна уу?")) {
        axios
          .delete(`${url}/${deleting}`)
          .then(() => {
            mutate(url);
            removeQuery("deleting");
            toast.success("Амжилттай устгалаа");
          })
          .catch(() => {
            removeQuery("deleting");
            toast.success("Устгахад алдаа гарлаа");
          });
      } else {
        removeQuery("deleting");
      }
    }
    if (editing && editing !== "new") {
      setLoading(true);
      axios
        .get(`${url}/${editing}`)
        .then((res) => {
          setEditingItem(res.data.result);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [searchParams]);

  const handleClose = () => {
    removeQuery("editing");
    setEditingItem(undefined);
  };

  const handleCreate = () => addQuery("editing", "new");

  const handleSubmit = () => {
    mutate(url);
    toast.success(`Амжилттай ${editingItem ? "заслаа" : "нэмлээ"}`);
    handleClose();
  };

  const handlePageChange = useCallback((value: number) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((value: number) => {
    setRowsPerPage(value);
  }, []);
  return { rowsPerPage, page, totalItems: data.length, isLoading: isLoading && loading, items: data, pageItems, handleRowsPerPageChange, handlePageChange, handleSubmit, handleCreate, isCreating: editing === "new", editingItem, handleClose };
};
