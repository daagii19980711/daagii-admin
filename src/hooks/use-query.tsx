import { usePathname, useRouter, useSearchParams } from "next/navigation";

export const useQuery = () => {
  const searchParams = new URLSearchParams(useSearchParams());
  const pathName = usePathname();
  const router = useRouter();
  const addQuery = (key: string, value: string) => {
    if (searchParams.has(key)) {
      searchParams.delete(key);
    }
    searchParams.append(key, value);
    router.push(`${pathName}?${searchParams.toString()}`);
  };
  const removeQuery = (key: string) => {
    searchParams.delete(key);
    router.push(`${pathName}?${searchParams.toString()}`);
  };
  return { addQuery, removeQuery };
};
