import { UserContext } from "@admin/contexts/user-context";
import { useContext } from "react";

export const useUser = () => {
  const userContext = useContext(UserContext);
  return userContext;
};
