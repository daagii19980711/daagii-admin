import { FingerPrintIcon, NewspaperIcon, PhotoIcon, RectangleStackIcon, TagIcon, UsersIcon } from "@heroicons/react/24/solid";
import { SvgIcon } from "@mui/material";

export const items = [
  {
    title: "Мэдээ мэдээлэл",
    path: "/dashboard/articles",
    icon: (
      <SvgIcon fontSize="small">
        <NewspaperIcon />
      </SvgIcon>
    ),
  },
  {
    title: "Ангилал",
    path: "/dashboard/categories",
    icon: (
      <SvgIcon fontSize="small">
        <TagIcon />
      </SvgIcon>
    ),
  },
  {
    title: "Хэрэглэгчид",
    path: "/dashboard/users",
    icon: (
      <SvgIcon fontSize="small">
        <UsersIcon />
      </SvgIcon>
    ),
  },
  {
    title: "Зургын сан",
    path: "/dashboard/images",
    icon: (
      <SvgIcon fontSize="small">
        <PhotoIcon />
      </SvgIcon>
    ),
  },
  // {
  //   title: "Төрөл",
  //   path: "#",
  //   icon: (
  //     <SvgIcon fontSize="small">
  //       <RectangleStackIcon />
  //     </SvgIcon>
  //   ),
  // },

  // {
  //   title: "Хэрэглэгчийн лог",
  //   path: "#",
  //   icon: (
  //     <SvgIcon fontSize="small">
  //       <FingerPrintIcon />
  //     </SvgIcon>
  //   ),
  // },
];
